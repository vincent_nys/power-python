.. Power Python documentation master file, created by
   sphinx-quickstart on Sat Oct 20 11:38:47 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Power Python!
========================

Power Python is a book that aims to take your Python skills to the next level.

The book should by readable front-to-back as long as you have a working
knowledge of Python. Unless the text states otherwise, code examples should
be run against the latest version of Python 3. At the time of writing, the
latest stable release is 3.3.

Contents:

.. toctree::
   :maxdepth: 2
   
   decorators
   generators
   context managers
   descriptors
   mocking

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

