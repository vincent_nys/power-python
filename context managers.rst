****************
Context Managers
****************

The Uninitiated
===============
You know how *every single introduction to IO* stresses how you should **close
files** when you're done with them? Or how you should do the same with 
**database connections**? Or how **transactions** are essentially atomic units
of code? Look at the block of code using the file or the connection as a
context in which certain operations take place. Give transactions a thought for
a moment, too. These contexts can be made tangible using a context manager,
which is what we'll be looking at in this chapter. Even if you don't care about
the examples listed above, you may want to read on: context managers are useful
for many types of recurring structures in code blocks.

Exercises
---------
#. Google other uses of context managers in Python. The
   :ref:`further-reading` section also contains a few.

.. _context-managers-apprentice:

The Apprentice
==============
Let's start by looking at the most common syntax related to context managers::

  with EXPR as VAR:
      BLOCK

There are two keywords here: `with` and `as`. The use of `with` indicates that
a context manager is being created. What I have called `VAR` can be considered
as the "working object". The most common example of such an object is
probably the file handle. Let's say you're an
entomologist and you've got a list of insects that you'd like to print out.
Then you would write this sort of thing::

  with open('bug_list.txt') as fh:
      for line in fh.readlines():
          print(line.strip()) # print already inserts newline, so strip those

If you don't need a "working object", you can just use the following syntax::

   with EXPR:
       BLOCK

The cool thing about context managers is that they make the scope you're
reasoning about clearer and that they can perform cleanup automatically, even
if an exception occurs. All this is essentially the result of two magic
methods of the context manager being called. These are called `__enter__` and
`__exit__`. The former is called at the beginning of the block demarcated
by `with` and returns the "working object". The latter is called at the end of
the same block and deals with any exceptions that might have occurred.
Make sure you get that:
`__enter__` returns the "working object", not the context manager.

Now, when I say that the `__exit__` method deals with any exceptions that might
have occurred, I don't mean that it just swallows them. The method's signature
is enlightening::

  def __exit__(self, type, value, traceback):
      self._restore() # just go back to the pre-context-manager state

You get the same three parameters here that you get when calling `this guy <http://docs.python.org/3/library/sys.html#sys.exc_info>`_.
Based on that information, the context manager can decide whether it can handle
any exception that may have occurred. Maybe it can resolve it entirely. In that
case, the `__exit__` method should return `True`. If it can't resolve it, it
should return `False`, so that the exception will propagate upwards.

Here's an example. Suppose we're planning a virtual heist. We want
to steal a number of rubies and diamonds from a house. You don't
just do that sort of thing unprepared. You need a stakeout. With the
stakeout monitoring for alarms and whatnot, the person doing the actual
burgling can go in. The whole thing looks something like this::

    import random
    
    class AlarmError(Exception): pass
    
    class House:
    
        def __init__(self, no_diamonds=0, no_rubies=0):
            self.no_diamonds = no_diamonds
            self.no_rubies = no_rubies
    
        def _yield_gems(self, alarm_prob, gem_attr):
            if random.random() < alarm_prob:
                raise AlarmError
            else:
                temp = getattr(self, gem_attr)
                setattr(self, gem_attr, 0)
                return temp
    
        def yield_diamonds(self):
            return self._yield_gems(0.2, 'no_diamonds')
    
        def yield_rubies(self):
            return self._yield_gems(0.1, 'no_rubies')
    
    class Burglar:
    
        def __init__(self, house):
            self.house = house
            self.diamonds = 0
            self.rubies = 0
    
        def grab_diamonds(self):
            self.diamonds += self.house.yield_diamonds()
    
        def grab_rubies(self):
            self.rubies += self.house.yield_rubies()
    
    class StakeOut:
    
        def __init__(self, house):
            self.house = house
    
        def __enter__(self):
            return Burglar(self.house)
    
        def __exit__(self, exc_type, value, traceback):
            if exc_type is None:
                print('Home free!')
            elif exc_type is AlarmError:
                print('Drop the loot and run!')
                return True
    
    my_house = House(no_diamonds = 3, no_rubies = 7)
    with StakeOut(my_house) as burglar:
        burglar.grab_diamonds()
        burglar.grab_rubies()

.. todo:: go through the example step by step


Exercises
---------
#. Drill the syntax for context manager objects. What methods are involved and
   what do they do? What is the gist of the `with` statement?
#. Come up with another example. Write it out in its entirety and make sure
   it runs as you expect it to.

The Journeyman
==============
Use Cases
---------

`@contextmanager`
-----------------
Magic methods. They're among Python's most useful tricks, but they're also kind
of a wart on the Python syntax. Writing `__enter__` and `__exit__` methods
for new context managers isn't particularly appealing, but there's a more
convenient way to do this, using the `@contextmanager` decorator from the
`contextlib` module. At the time of writing, I haven't covered decorators yet,
but you don't need to know how they work to be able to use them. They basically
graft additional functionality onto an existing object, which, in the case of
function decorators, is a function.

Here's a clear example, drawn from the original documentation (which states
that this is not really a recommended way of generating HTML)::

  from contextlib import contextmanager

  @contextmanager
  def tag(name):
      print("<%s>" % name)
      yield
      print("</%s>" % name)

That'll create `__enter__` and `__exit__` methods for you automatically. The
way it works is like this: the decorator assumes that it is applied to a
generator function. This generator function should yield *exactly* one value,
which is why the example contains only one `yield` and no loop structure.
This value can be `None`, because you don't always want to use what is yielded
as part of a context. Again, the example illustrates this. As a result, the
`with` statement won't be accompanied by an `as`. So essentially the part up
to the `yield` statement makes up the `__enter__` method, with the assignment
of that method's result to the variable preceded by `as` as the last thing
that is done before the managed block begins to execute. As you can guess,
once the managed block has been executed, the generator picks up where it left
off, so that constitutes the `__exit__` method.

That's the basic idea, but exception handling does make things a bit more
complicated. If an exception occurs inside the block and it isn't caught,
a bit of magic (which will be explained in the 
:ref:`inner workings <inner-workings>` section)
causes the exception to be raised at the location of the `yield`. In other
words, from a flow control point of view, it will look as if the `yield`
is a function that raises the exception. So if you know how your context
manager is going to be used, you can take care of possible exceptions. For
instance, if it yields a file handle, you know that certain errors will pop
up sooner or later. So you should put the `yield` inside the scope of a `try`
statement and handle exceptions using `except` and handle general-purpose
cleanup (e.g. closing the file) in a `finally` block. Also note that, if your
context manager cannot eliminate the problem indicated by the exception with
absolute certainty, it should re-raise that exception.

Suppose you've got a context manager that's only meant to handle some kind
of exception and that can handle it with certainty. That'd look like this::

  try:
      yield
  except SomeError:
      print('Some error occurred. Consider it handled.')
      return True

.. _one-off-context-managers:

Something else worth noting here is that, under normal circumstances,
`@contextmanager` gives you a context manager that you can use once. That is,
you write::

  with bar() as foo:
      baz()

Again, the part up to the `yield` is essentially the `__enter__`, the part
that comes after is the `__exit__` and the block of code you write is executed
in between. If you use `bar` as a context manager, that function will be
invoked each time you use it as a context manager. So it is essentially turned
into a new context manager each time, giving you "one-off" context managers.
With a manually coded context manager, you don't have that restriction. That
is, you can use the same object as a context manager in multiple locations.
That gives you the added power to do whatever you want with that object,
but it seems that generator-based context managers are still the most common
form of context manager you'll come across.

Separating Exception Logic from Application Logic
-------------------------------------------------

There's a good illustration of how you can separate exception logic from
application logic over at `Hoarded Homely Hints <http://dietbuddha.blogspot.be/2012/12/52python-encapsulating-exceptions-with.html>`_.
I could just leave it at that, but I'm writing this book to teach myself, so
I won't. Here's my own variation on his example.

Suppose you've got a band. Each song is a method. Normally, you just play
each song in turn, but if some jerk messes with the P.A. system, you need
a backup plan. Same thing if a band member, all of a sudden, just can't
stop sneezing. Also, it turns out that people only start messing with the
P.A. system for songs whose title starts with a letter in the second part
of the alphabet, whereas band members only sneeze if the song starts with
a letter in the first part of the alphabet.
Here's a first attempt at representing that::

  import random

  class SneezeError(Exception): pass
  class PAJerkError(Exception): pass

  def song(title):
      print(title)
      print('BABY!')
      occurrence = random.random()
      first_letter = ord(title[0].lower())
      if occurrence < 0.05 and first_letter <= ord('m'):
          raise SneezeError
      elif occurrence < 0.05:
          raise PAJerkError
      print(title)

  try:
      song('Children of the Night')
  except SneezeError:
      tell_joke()

  try:
      song('Crying in the Rain')
  except SneezeError:
      tell_joke()

  try:
      song("Saints an' Sinners")
  except PAJerkError:
      get_angry()

You need four lines for each song. You could easily write a "secure" function
that does all the exception handling, but then you can't re-use the context
manager part alone. So here's another representation. It's actually slightly
longer, but it grows at a slower tempo::

  from contextlib import contextmanager
  import random

  class SneezeError(Exception): pass
  class PAJerkError(Exception): pass

  def song(title):
      print(title)
      print('BABY!')
      occurrence = random.random()
      first_letter = ord(title[0].lower())
      if occurrence < 0.05 and first_letter <= ord('m'):
          raise SneezeError
      elif occurrence < 0.05:
          raise PAJerkError
      print(title)

  @contextmanager
  def _sneeze_handler():
      try:
          yield
      except SneezeError:
          tell_joke()

  @contextmanager
  def _pa_handler():
      try:
          yield
      except PAJerkError:
          get_angry()

  with _low_alphabet_handler():
      song('Children of the Night')

  with _sneeze_handler():
      song('Crying in the Rain')

  with _pa_handler():
      song("Saints an' Sinners")

What's the advantage of being able to re-use context managers? Well, there
might be different situations where these things are handy. Maybe band
members start sneezing uncontrollably while the frontman is talking to the
crowd between songs. Maybe people are inclined to mess with the P.A. during
sound checking. In that case, you can apply the same context managers in
different situations.

Exercises
---------
#. A way to emulate "labelled breaks" using context managers is discussed at
   `Keegan Carruthers-Smith's blog <http://people.cs.uct.ac.za/~ksmith/2011/labelled-break-with-contex-manager.html>`_.
   A labelled break is pretty similar to a `goto` statement, but it's a bit
   more structured. Essentially, you place a label somewhere before or inside
   a loop structure and you invoke something like `break mylabel` to jump to
   that label once you've done enough looping (e.g. when you've found what you
   were looking for in a big nested structure). Labelled breaks are not
   supported in Python, but you can write a context manager that emulates them.
   The trick is to exploit the fact that exceptions propagate upwards and can
   be handled by `__exit__`. Can you write this context manager? A complete
   implementation can be found on the blog I linked to.

#. `Matthias Friedrich <http://blog.mafr.de/2010/01/23/fun-with-context-managers/>`_
   came up with the idea of writing a quick stopwatch mechanism that uses a
   context manager for profiling. I haven't looked at Python's dedicated
   profiling library yet, so there may be a more elegant solution, but this
   makes for a good, short exercise. Use `@contextmanager` to implement one of
   these guys. Matthias's implementation makes it possible to name the stopwatch
   and to set a flag indicating whether you want to swallow exceptions.

The Master
==========
Advanced Use Cases
------------------
`ContextDecorator`: Context Managers as Decorators
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Context decorators are conceptually simple, but you probably won't see them
being used that often. The idea is simply that some functions will always be
used in the same context, so that stating the context explicitly becomes kind
of redundant. Instead, you can decorate the function with the context.
Here's how you apply this::

  @mycontext()
  def function():
    print("Hello!")

To be able to do this, you need a context manager that derives from
`contextlib.ContextDecorator`. This can be a manually coded manager with the
appropriate magic methods and `ContextDecorator` as one of its superclasses,
or it can be one that was built using `@contextmanager`. In fact, all context
managers built with the decorator can be used like this without modification.
This is possible despite the fact that `@contextdecorator` gives you a context
manager that can only be used once, as mentioned in the notes on
:ref:`one-off context managers <one-off-context-managers>`. The reason for
this is that the decorated function is equivalent to::

  def function():
    with mycontext():
      print("Hello!")

The Python documentation has this little caveat for us, though:

   As the decorated function must be able to be called multiple times, the
   underlying context manager must support use in multiple `with` statements.
   If this is not the case, then the original construct with the explicit
   `with` statement inside the function should be used.

You might need to use your imagination to think about code that doesn't satisfy
this requirement. So just keep that little caveat in mind.

.. note::

   When writing a context manager, think about what happens if you re-use it.
   Normally speaking, it should be possible to use the same context manager
   in multiple `with` statements, or it should be a one-off context manager.

.. _inner-workings:

Inner Workings
--------------
Now, we'll delve into what really makes context managers tick. You don't need
to know all this to be able to use them, so if you're fairly new to Python,
you might want to hold off for a while. That said, let's dig in.
This section will be based on several Python enhancement
Proposals and will explain the entire flow of control created by context
managers. Unlike the PEPs, however, it won't go into "proposed alternatives" or
historical details.

.. todo:: understand PEP by reading up on contextlib.contextmanager
.. todo:: link to PEPs in text

For this, the `generator.throw` function was introduced. Here is how it is
described in PEP342:

  `g.throw(type, value, traceback)` causes the specified exception to
  be thrown at the point where the generator `g` is currently
  suspended (i.e. at a yield-statement, or at the start of its
  function body if `next()` has not been called yet).  If the
  generator catches the exception and yields another value, that is
  the return value of `g.throw()`.  If it doesn't catch the exception,
  the `throw()` appears to raise the same exception passed it (it
  "falls through").  If the generator raises another exception (this
  includes the `StopIteration` produced when it returns) that
  exception is raised by the `throw()` call.  In summary, `throw()`
  behaves like `next()` or `send()`, except it raises an exception at the
  suspension point.  If the generator is already in the closed
  state, `throw()` just raises the exception it was passed without
  executing any of the generator's code.

With this function in place, another new method for generators can be built.
Here is `generator.close` as it is sketched in PEP342::

  def close(self):
      try:
          self.throw(GeneratorExit)
      except (GeneratorExit, StopIteration):
          pass
      else:
          raise RuntimeError("generator ignored GeneratorExit")
      # Other exceptions are not caught

Note that `GeneratorExit` differs from `StopIteration` in that the latter
signals that there simply are no more elements to be generated.

Exercises
---------
#. Read `PEP 310 <http://www.python.org/dev/peps/pep-0310/>`_,
   `PEP 340 <http://www.python.org/dev/peps/pep-0340/>`_ and finally
   `PEP 343 <http://www.python.org/dev/peps/pep-0343/>`_. Read them in that
   order, look up anything that is unclear and re-read until you can follow
   along perfectly. Make a comparison between the different proposals.

#. Fork the source for this chapter, try to improve upon it and make a pull
   request.

.. _further-reading:

Further Reading
===============
* `With Statement Context Managers
  <http://docs.python.org/release/3.2.3/reference/datamodel.html#context-managers>`_
* `Context Manager Types
  <http://docs.python.org/release/3.2.3/library/stdtypes.html#typecontextmanager>`_
* `PEP 343
  <http://www.python.org/dev/peps/pep-0343/>`_
* `Python context managers: they are easy!
  <http://lateral.netmanagers.com.ar/weblog/posts/BB963.html>`_
* `Managing Contexts: The with Statement
  <http://www.itmaybeahack.com/book/python-2.6/html/p03/p03c07_contexts.html>`_
* `This is Python: context managers and their use
  <http://requires-thinking.blogspot.be/2009/02/this-is-python-context-managers-and.html>`_
* `contextlib
  <http://docs.python.org/3.2/library/contextlib.html?highlight=contextlib>`_
* `The Python “with” Statement by Example
  <http://preshing.com/20110920/the-python-with-statement-by-example>`_
